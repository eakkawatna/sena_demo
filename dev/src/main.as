package
{
	import com.sleepydesign.display.SDStageSprite;
	import com.sleepydesign.robotlegs.shells.view.ShellView;
	import com.sleepydesign.system.DebugUtil;

	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.utils.getQualifiedClassName;


	public class main extends SDStageSprite
	{

		protected const _ID:String = getQualifiedClassName(this);
		private var _appScale:Number;
		private static var _shellView:ShellView;

		public static function get shellView():ShellView
		{
			return _shellView;
		}

		public function get ID():String
		{
			return _ID;
		}

		public function main()
		{
			initStage();
			initModule();

			super();
		}

		private function initModule():void
		{
			Config.init();
		}

		override protected function onStage():void
		{
			_shellView = new ShellView();
			addChild(_shellView);

			_shellView.scaleX = _shellView.scaleY = Config.ratio;

			Jojo.init(this.stage);
			Jojo.startBreeze();
		}

		private function initStage():void
		{
			stage.align = StageAlign.TOP_LEFT;
			stage.scaleMode = StageScaleMode.NO_SCALE;
		}

		private function initDebug():void
		{
			DebugUtil.init(this);
		}
	}
}


