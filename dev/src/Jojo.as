package
{
	import com.greensock.TweenLite;
	import com.sleepydesign.system.DebugUtil;
	import com.sleepydesign.utils.AlignUtil;

	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.Stage;
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.geom.Rectangle;
	import flash.utils.Timer;

	import org.osflash.signals.Signal;

	public class Jojo
	{
		private static var _stage:Stage;
		private static var _timer:Timer;
		private static var _target:DisplayObject;

		private static var _isRunning:Boolean;

		/**
		 * Must init before doing someting else
		 * @param stage
		 *
		 */
		public static function init(stage:Stage):void
		{
			_stage = stage;
		}

		/**
		 * Will dispatch every sec.
		 */
		public static var breezeSignal:Signal = new Signal(Number);

		/**
		 * Will dispatch every frame
		 */
		public static var runSignal:Signal = new Signal();

		/**
		 * Will call every frame
		 * @param stage
		 */
		public static function startRun():Signal
		{
			if (!_stage)
				throw new Error("Must init stage first ");

			// every frame
			if (!_isRunning)
				_stage.addEventListener(Event.ENTER_FRAME, running);

			_isRunning = true;

			return runSignal;
		}

		private static function running(event:Event):void
		{
			runSignal.dispatch();
		}

		/**
		 * Will remove enterframe
		 * @param stage
		 *
		 */
		public static function stopRun():Signal
		{
			if (!_stage)
				throw new Error("Must init stage first ");

			_stage.removeEventListener(Event.ENTER_FRAME, running);
			_isRunning = false;

			return runSignal;
		}

		/**
		 * Will call every desire millisec
		 * @param millisec
		 */
		public static function startBreeze(millisec:Number = 1000):Signal
		{
			if (_timer)
				return breezeSignal;

			_timer = new Timer(millisec);
			_timer.addEventListener(TimerEvent.TIMER, onBreeze);
			_timer.start();

			return breezeSignal;
		}

		private static function onBreeze(event:TimerEvent):void
		{
			breezeSignal.dispatch(event.target.currentCount);
		}

		/**
		 * Will remove global timer
		 * @param millisec
		 *
		 */
		public static function stopBreeze():Signal
		{
			_timer.removeEventListener(TimerEvent.TIMER, onBreeze);
			_timer.stop();
			_timer = null;

			return breezeSignal;
		}

		public static function callStand(rect:Rectangle, target:DisplayObject, callback:Function = null):void
		{
			DebugUtil.trace(" * callStand : " + callback);

			if (_target)
				removeStand();

			_target = target;

			var dimBitmap:Bitmap = new Bitmap(new BitmapData(rect.width, rect.height, true, 0xFF000000));
			var stageBitmap:Bitmap = new Bitmap(dimBitmap.bitmapData.clone());
			stageBitmap.bitmapData.draw(_stage, null, null, null, rect);
			dimBitmap.alpha = 0.0;

			if (callback is Function)
				TweenLite.to(dimBitmap, .25, {alpha: .75, onComplete: function():void
				{
					DebugUtil.trace(" ^ callStand.complete");
					callback();
				}});
			else
				TweenLite.to(dimBitmap, .25, {alpha: .75});

			var visibles:Array = [];
			for (var i:int = 0; i < _stage.numChildren; i++)
			{
				visibles[i] = _stage.getChildAt(i).visible;
				_stage.getChildAt(i).visible = false;
			}

			_stage.addChild(stageBitmap);
			_stage.addChild(dimBitmap);
			_stage.addChild(target);

			AlignUtil.align(AlignUtil.MIDDLE_CENTER, target, rect);

			target.addEventListener(Event.REMOVED_FROM_STAGE, function onRemoved():void
			{
				target.removeEventListener(Event.REMOVED_FROM_STAGE, onRemoved);

				//TweenLite.to(dimBitmap, .25, {alpha: 0, onComplete: function():void
				//{
				for (var i:int = 0; i < _stage.numChildren; i++)
					_stage.getChildAt(i).visible = visibles[i];

				stageBitmap.bitmapData.dispose();
				_stage.removeChild(stageBitmap);
				stageBitmap = null;

				dimBitmap.bitmapData.dispose();
				_stage.removeChild(dimBitmap);
				dimBitmap = null;
				//}});
			});
		}

		public static function removeStand(callback:Function = null):void
		{
			DebugUtil.trace(" * removeStand");

			if (_target && _target.parent)
				_target.parent.removeChild(_target);

			_target = null;

			if (callback is Function)
				callback();
		}

		/**
		 * Will freeze for desired millisecond and do callback.
		 *
		 * @param millisec
		 * @param callback
		 *
		 */
		public static function freeze(millisec:Number, callback:Function = null):void
		{
			var timer:Timer = new Timer(millisec, 1);
			timer.addEventListener(TimerEvent.TIMER, function onFreezed():void
			{
				timer.stop();
				timer.removeEventListener(TimerEvent.TIMER, onFreezed);

				if (callback is Function)
					callback();
			});

			timer.start();
		}

		/**
		 * Will map signal and auto run
		 * @param onRun
		 * @return runSignal
		 *
		 */
		public static function run(onRun:Function = null):Signal
		{
			startRun().add(onRun);

			return runSignal;
		}

		/**
		 * Will remove run signal
		 * @param onRun
		 * @return runSignal
		 *
		 */
		public static function norun(onRun:Function = null):Signal
		{
			runSignal.remove(onRun);

			return runSignal;
		}
	}
}
