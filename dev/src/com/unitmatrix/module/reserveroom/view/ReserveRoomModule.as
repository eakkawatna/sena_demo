package com.unitmatrix.module.reserveroom.view
{
	import com.bit101.components.PushButton;
	import com.sleepydesign.robotlegs.apps.view.IView;
	import com.sleepydesign.robotlegs.modules.ModuleBase;
	import com.unitmatrix.module.reserveroom.ReserveRoomContext;

	import flash.display.Bitmap;
	import flash.display.SimpleButton;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.text.StageText;

	import assets.floorplan_sp;

	import org.robotlegs.core.IInjector;

	public class ReserveRoomModule extends ModuleBase
	{

		private var login:Sprite;

		private var username_tf:StageText;
		private var password_tf:StageText;
		private var login_btn:SimpleButton;

		private var room_conatiner:Sprite;
		private var _skin:Sprite;

		private var _room:Sprite;

		[Inject]
		public function set parentInjector(value:IInjector):void
		{
			_context = new ReserveRoomContext(this, value);
		}

		override public function create():void
		{
			/*this.graphics.beginFill(0xffffff, 1.0);
			this.graphics.drawRect(0, 0, stage.stageWidth, stage.stageHeight);
			this.graphics.endFill();

			var back_btn:PushButton = new PushButton(this, 0, 0, "Back");
			back_btn.name = "back_btn";

			var building_container:Sprite = new Sprite();

			building_container.x = 50;
			building_container.y = 100;

			addChild(building_container);

			var ypos:int = 0
			for (var i:int = 16; i >= 1; i--)
			{
				var floor_btn:PushButton = new PushButton(building_container, 0, ypos, "floor " + i);
				floor_btn.name = "floor_" + i;
				ypos += floor_btn.height;
			}*/

			_skin = new assets.floorplan_sp;
			addChild(_skin);
		}

		public function createRoomSlot():void
		{
			/*if (room_conatiner)
				removeChild(room_conatiner);

			room_conatiner = new Sprite();
			addChild(room_conatiner);

			room_conatiner.x = 200;
			room_conatiner.y = 100;

			var xpos:int = 0;
			var ypos:int = 0;
			for (var i:int = 0; i < 2; i++)
			{
				xpos = 0;
				for (var j:int = 0; j < 5; j++)
				{
					var room_btn:PushButton = new PushButton(room_conatiner, xpos, ypos, "Room " + (j + (5 * i) + 1));
//					if (i == 1 && j == 1)
//						room_btn.enabled = false;
//					else if (i == 1 && j == 4)
//						room_btn.enabled = false;

					if (Math.random() * 10 < 3)
						room_btn.enabled = false;

					room_btn.name = "room_" + (j + (5 * i) + 1);
					xpos += room_btn.width;
				}

				ypos += 20;
			}
*/
			if (!_room)
			{
				_room = new assets_select_floorplan_sp;
				_room.name = "room";
				addChild(_room);

				_room.x = 795;
				_room.y = 235;
			}
		}

		protected function onActiveText(event:Event):void
		{
			var textField:StageText = event.target as StageText;
			if (textField.text != "")
				textField.text = "";

		}

		public function get userName():String
		{
			return username_tf.text;
		}

		public function get password():String
		{
			return password_tf.text;
		}

		private function resize():void
		{
			login.scaleX = login.scaleY = Config.ratio;
			login.x = Config.GUI_SIZE.width * 0.5 - login.width * 0.5;
			login.y = Config.GUI_SIZE.height * 0.5 - login.height * 0.5;
		}

		override public function activate(onActivated:Function, prevView:IView = null):void
		{
			super.activate(onActivated, prevView);
		}

		override public function deactivate(onDeactivated:Function, nextView:IView = null):void
		{
			dispose();
			super.deactivate(onDeactivated, nextView);
		}

		public function set showText(value:Boolean):void
		{
			username_tf.visible = value;
			password_tf.visible = value;
		}

		override public function dispose():void
		{
			if (username_tf)
				username_tf.dispose();
			if (password_tf)
				password_tf.dispose();
			destroy();
			super.dispose();
		}
	}
}
