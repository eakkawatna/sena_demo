package com.unitmatrix.module.reserveroom.view
{
	import com.sleepydesign.lightboxs.signals.LightBoxSignal;
	import com.sleepydesign.lightboxs.view.LightBoxModule;
	import com.sleepydesign.robotlegs.apps.model.AppModel;
	import com.unitmatrix.core.component.DialogComponent;
	import com.unitmatrix.core.model.CoreModel;
	import com.unitmatrix.core.model.data.UserData;
	import com.unitmatrix.module.reserveroom.model.ReserveRoomModel;
	import com.unitmatrix.module.reserveroom.model.data.ReserveRoomData;
	import com.unitmatrix.module.reserveroom.signals.ReceivedLoginResult;
	import com.unitmatrix.module.roomdetail.view.RoomDetailModule;

	import flash.events.MouseEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;

	import net.pirsquare.m.MobileFileUtil;

	import org.osflash.signals.Signal;
	import org.robotlegs.utilities.modular.mvcs.ModuleMediator;

	public class ReserveRoomMediator extends ModuleMediator
	{
		// external ------------------------------------------------------------------

		[Inject]
		public var appModel:AppModel;

		[Inject]
		public var coreModel:CoreModel;

		[Inject]
		public var lightBoxSignal:LightBoxSignal;

		// internal ------------------------------------------------------------------

		[Inject]
		public var module:ReserveRoomModule;
		[Inject]
		public var model:ReserveRoomModel;

		[Inject]
		public var receiveLoginSignal:ReceivedLoginResult;


		// create ------------------------------------------------------------------
		private var _dialogComponent:DialogComponent;

		private var _dialogSignal:Signal;

		override public function onRegister():void
		{
			module.create();

			eventMap.mapListener(module, MouseEvent.MOUSE_UP, onMouseUp);
		}

		private function onMouseUp(event:MouseEvent):void
		{
			if (event.target.name == "back_btn")
				appModel.viewManager.viewID = Config.M20_HOME_MODULE;
			else if (event.target.name.indexOf("floor") != -1)
				module.createRoomSlot();
			else if (event.target.name.indexOf("room") != -1)
			{
				coreModel.RoomName = event.target.name;
				module.addChild(new RoomDetailModule);
			}

		}


		// destroy ------------------------------------------------------------------

		override public function onRemove():void
		{
			eventMap.unmapListener(module, MouseEvent.MOUSE_UP, onMouseUp);

			appModel.viewManager.removeViewByID(module.ID);
			appModel = null;
			module = null;
		}
	}
}
