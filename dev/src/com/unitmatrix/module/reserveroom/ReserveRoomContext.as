package com.unitmatrix.module.reserveroom
{
	import com.unitmatrix.module.reserveroom.controller.RequestReserveRoomCommand;
	import com.unitmatrix.module.reserveroom.model.ReserveRoomModel;
	import com.unitmatrix.module.reserveroom.remote.services.ReserveRoomService;
	import com.unitmatrix.module.reserveroom.signals.ReceivedLoginResult;
	import com.unitmatrix.module.reserveroom.signals.RequestLoginSignal;
	import com.unitmatrix.module.reserveroom.view.ReserveRoomMediator;
	import com.unitmatrix.module.reserveroom.view.ReserveRoomModule;
	import com.sleepydesign.robotlegs.modules.ModuleBase;

	import flash.display.DisplayObjectContainer;
	import flash.utils.getDefinitionByName;

	import org.robotlegs.core.IInjector;
	import org.robotlegs.utilities.modular.mvcs.ModuleContext;


	public class ReserveRoomContext extends ModuleContext
	{
		public function ReserveRoomContext(contextView:DisplayObjectContainer, injector:IInjector)
		{
			super(contextView, true, injector);
		}

		override public function startup():void
		{
			injector.mapSingleton(ReserveRoomService);
			injector.mapSingleton(ReserveRoomModel);
			injector.mapSingleton(ReceivedLoginResult);

			signalCommandMap.mapSignalClass(RequestLoginSignal, RequestReserveRoomCommand, false, ['username', 'password']);

			mediatorMap.mapView(ReserveRoomModule, ReserveRoomMediator);

			super.startup();
		}

		override public function dispose():void
		{
			mediatorMap.unmapView(getDefinitionByName(ModuleBase(contextView).ID) as Class);
			mediatorMap.removeMediatorByView(contextView);

			super.dispose();
		}
	}
}
