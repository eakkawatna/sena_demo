package com.unitmatrix.module.reserveroom.signals
{
	import org.osflash.signals.Signal;
	
	public class RequestLoginSignal extends Signal
	{
		public function RequestLoginSignal()
		{
			super(String,String)
		}
	}
}