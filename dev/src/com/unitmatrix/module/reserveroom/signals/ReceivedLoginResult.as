package com.unitmatrix.module.reserveroom.signals
{
	import com.unitmatrix.core.model.data.UserData;
	
	import org.osflash.signals.Signal;

	public class ReceivedLoginResult extends Signal
	{
		public function ReceivedLoginResult()
		{
			super(String, UserData);
		}
	}
}
