package com.unitmatrix.module.home
{

	import com.unitmatrix.module.home.view.HomeMediator;
	import com.unitmatrix.module.home.view.HomeModule;
	import com.sleepydesign.robotlegs.modules.ModuleBase;

	import flash.display.DisplayObjectContainer;
	import flash.utils.getDefinitionByName;

	import org.robotlegs.core.IInjector;
	import org.robotlegs.utilities.modular.mvcs.ModuleContext;

	public class HomeContext extends ModuleContext
	{
		public function HomeContext(contextView:DisplayObjectContainer, injector:IInjector)
		{
			super(contextView, true, injector);
		}

		override public function startup():void
		{
			mediatorMap.mapView(HomeModule, HomeMediator);



			super.startup();
		}

		override public function dispose():void
		{
			mediatorMap.unmapView(getDefinitionByName(ModuleBase(contextView).ID) as Class);
			mediatorMap.removeMediatorByView(contextView);



			super.dispose();
		}
	}
}
