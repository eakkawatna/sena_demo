package com.unitmatrix.module.home.view
{
	import com.bit101.components.PushButton;
	import com.sleepydesign.robotlegs.apps.view.IView;
	import com.sleepydesign.robotlegs.modules.ModuleBase;
	import com.unitmatrix.module.home.HomeContext;

	import flash.display.Sprite;
	import flash.geom.Rectangle;
	import flash.media.StageWebView;

	import assets.fb_sp;
	import assets.home_sp;

	import org.robotlegs.core.IInjector;


	public class HomeModule extends ModuleBase
	{

		private var _fb_d:Sprite;

		private var _html:StageWebView;

		[Inject]
		public function set parentInjector(value:IInjector):void
		{
			_context = new HomeContext(this, value);
		}

		override public function create():void
		{

			var skin:Sprite = new assets.home_sp;
			addChild(skin);
			/*var xpos:int = 100;
			var ypos:int = 200;
			var reserve_btn:PushButton = new PushButton(this, xpos, ypos, "reserve room");
			reserve_btn.name = "reserve_btn";
			var questionnaire_btn:PushButton = new PushButton(this, xpos, ypos + 50, "questionnaire");
			questionnaire_btn.name = "questionnaire_btn";*/

			_fb_d = new assets.fb_sp;
			addChild(_fb_d);

			_html = new StageWebView(true);
			_html.stage = stage;
			_html.viewPort = new Rectangle(440 * Config.ratio, 220 * Config.ratio, 1000 * Config.ratio, 650 * Config.ratio);
			_html.loadURL("http://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&width&layout=standard&action=like&show_faces=true&share=true&height=80&appId=1414107692186436");

		}



		override public function activate(onActivated:Function, prevView:IView = null):void
		{
			super.activate(onActivated, prevView);
		}

		override public function deactivate(onDeactivated:Function, nextView:IView = null):void
		{
			super.deactivate(onDeactivated, nextView);
		}

		override public function dispose():void
		{
			destroy();
			super.dispose();
		}

		public function removeFB():void
		{
			removeChild(_fb_d);
			_html.stage = null;
		}
	}
}
