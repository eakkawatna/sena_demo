package com.unitmatrix.module.roomdetail.view.component
{
	import com.sleepydesign.display.SDSprite;

	import flash.display.Sprite;
	import flash.text.TextField;

	import Tab.structure_sp;

	public class StructureComponent extends SDSprite
	{
		private var _skin:Sprite;

		public function StructureComponent(typeabbrev:String, piername:String, slopeprotectionname:String, surveytypename:String, leftroadside:String, rightroadside:String, bridgerailing:String)
		{
			_skin = new Tab.structure_sp;
			addChild(_skin);

			typeabbrev_tf.text = typeabbrev || "ไม่มีข้อมูล";
			piername_tf.text = piername || "ไม่มีข้อมูล";
			slopeprotectionname_tf.text = slopeprotectionname || "ไม่มีข้อมูล";
			surveytypename_tf.text = surveytypename || "ไม่มีข้อมูล";
			leftroadside_tf.text = leftroadside || "ไม่มีข้อมูล";
			rightroadside_tf.text = rightroadside || "ไม่มีข้อมูล";
			bridgerailing_tf.text = bridgerailing || "ไม่มีข้อมูล";
		}

		public function get typeabbrev_tf():TextField
		{
			return _skin.getChildByName("typeabbrev_tf") as TextField;
		}

		public function get piername_tf():TextField
		{
			return _skin.getChildByName("piername_tf") as TextField;
		}

		public function get slopeprotectionname_tf():TextField
		{
			return _skin.getChildByName("slopeprotectionname_tf") as TextField;
		}

		public function get surveytypename_tf():TextField
		{
			return _skin.getChildByName("surveytypename_tf") as TextField;
		}

		public function get leftroadside_tf():TextField
		{
			return _skin.getChildByName("leftroadside_tf") as TextField;
		}

		public function get rightroadside_tf():TextField
		{
			return _skin.getChildByName("rightroadside_tf") as TextField;
		}

		public function get bridgerailing_tf():TextField
		{
			return _skin.getChildByName("bridgerailing_tf") as TextField;
		}

	}
}
