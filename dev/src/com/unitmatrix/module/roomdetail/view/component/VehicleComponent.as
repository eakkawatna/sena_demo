package com.unitmatrix.module.roomdetail.view.component
{
	import com.unitmatrix.core.model.data.VehicleData;
	import com.sleepydesign.display.SDSprite;

	import flash.display.Sprite;
	import flash.text.TextField;

	import Tab.howto_sp;

	import __AS3__.vec.Vector;

	public class VehicleComponent extends SDSprite
	{
		private var _skin:Sprite;

		public function VehicleComponent(createdate:String, current:String, propertybottomname:String, aadtyear:String, aadttruck:String, aadt:String, vehicleDatas:Vector.<VehicleData>)
		{
			_skin = new Tab.howto_sp;
			addChild(_skin);

			createdate_tf.text = createdate || "ไม่มีข้อมูล";
			current_tf.text = current || "ไม่มีข้อมูล";
			propertybottomname_tf.text = propertybottomname || "ไม่มีข้อมูล";
			aadtyear_tf.text = aadtyear || "ไม่มีข้อมูล";
			aadttruck_tf.text = aadttruck || "ไม่มีข้อมูล";
			aadt_tf.text = aadt || "ไม่มีข้อมูล";

			var count:int = 1
			for each (var vd:VehicleData in vehicleDatas)
			{
				var tf_id:TextField = getVehicleqtyByNumber(count);
				tf_id.text = vd.qty || "ไม่มีข้อมูล";
				var tf_nm:TextField = getVehiclenmByNumber(count);
				tf_nm.text = vd.name || "ไม่มีข้อมูล";
				count++;
			}
		}

		public function get createdate_tf():TextField
		{
			return _skin.getChildByName("createdate_tf") as TextField;
		}

		public function get current_tf():TextField
		{
			return _skin.getChildByName("current_tf") as TextField;
		}

		public function get propertybottomname_tf():TextField
		{
			return _skin.getChildByName("propertybottomname_tf") as TextField;
		}

		public function get aadtyear_tf():TextField
		{
			return _skin.getChildByName("aadtyear_tf") as TextField;
		}

		public function get aadttruck_tf():TextField
		{
			return _skin.getChildByName("aadttruck_tf") as TextField;
		}

		public function get aadt_tf():TextField
		{
			return _skin.getChildByName("aadt_tf") as TextField;
		}

		public function getVehicleqtyByNumber(value:int):TextField
		{
			return _skin.getChildByName("vehicleqty_" + value) as TextField;
		}

		public function getVehiclenmByNumber(value:int):TextField
		{
			return _skin.getChildByName("vehiclenm_" + value) as TextField;
		}

	/*
			public function get vehicleqty_1():TextField
			{
				return _skin.getChildByName("vehicleqty_1") as TextField;
			}

			public function get vehicleqty_2():TextField
			{
				return _skin.getChildByName("vehicleqty_2") as TextField;
			}

			public function get vehicleqty_3():TextField
			{
				return _skin.getChildByName("vehicleqty_3") as TextField;
			}

			public function get vehicleqty_4():TextField
			{
				return _skin.getChildByName("vehicleqty_4") as TextField;
			}

			public function get vehicleqty_5():TextField
			{
				return _skin.getChildByName("vehicleqty_5") as TextField;
			}

			public function get vehicleqty_6():TextField
			{
				return _skin.getChildByName("vehicleqty_6") as TextField;
			}

			public function get vehicleqty_7():TextField
			{
				return _skin.getChildByName("vehicleqty_7") as TextField;
			}

			public function get vehicleqty_8():TextField
			{
				return _skin.getChildByName("vehicleqty_8") as TextField;
			}

			public function get vehicleqty_9():TextField
			{
				return _skin.getChildByName("vehicleqty_9") as TextField;
			}

			public function get vehicleqty_10():TextField
			{
				return _skin.getChildByName("vehicleqty_10") as TextField;
			}

			public function get vehicleqty_11():TextField
			{
				return _skin.getChildByName("vehicleqty_11") as TextField;
			}

			public function get vehicleqty_12():TextField
			{
				return _skin.getChildByName("vehicleqty_12") as TextField;
			}*/
	}
}
