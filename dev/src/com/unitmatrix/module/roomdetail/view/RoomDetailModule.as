package com.unitmatrix.module.roomdetail.view
{
	import com.bit101.components.Label;
	import com.freshplanet.lib.ui.scroll.mobile.ScrollController;
	import com.sleepydesign.display.DisplayObjectUtil;
	import com.sleepydesign.robotlegs.apps.view.IView;
	import com.sleepydesign.robotlegs.modules.ModuleBase;
	import com.unitmatrix.module.roomdetail.RoomDetailContext;

	import flash.display.Bitmap;
	import flash.display.DisplayObjectContainer;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.geom.Rectangle;

	import assets.unit_type_sp;

	import org.robotlegs.core.IInjector;

	public class RoomDetailModule extends ModuleBase
	{

		[Embed(source = '/room.png')]
		private var BG_CLASS:Class;
		private var bg:Bitmap = new BG_CLASS();

		private var _bridge_sp:Sprite;
		private var _selectMenu:int;


		private var contentScroll:ScrollController;
		private var _container:Sprite;
		private var _content:Sprite;
		private var _skin:Sprite;

		public function get selectMenu():int
		{
			return _selectMenu;
		}

		[Inject]
		public function set parentInjector(value:IInjector):void
		{
			_context = new RoomDetailContext(this, value);
		}

		public function createRoom(RoomName:String):void
		{
			var label:Label = new Label(this, 200, 200, RoomName);
		}

		override public function create():void
		{
			/*this.graphics.beginFill(0x000000, 0.5);
			this.graphics.drawRect(0, 0, stage.stageWidth, stage.stageHeight);
			this.graphics.endFill();

			var back_btn:PushButton = new PushButton(this, 0, 0, "close");
			back_btn.name = "back_btn";

			AlignUtil.align(AlignUtil.MIDDLE_CENTER, bg, new Rectangle(0, 0, stage.stageWidth, stage.stageHeight));
			addChild(bg);*/

			_skin = new assets.unit_type_sp;
			addChild(_skin);
		}

		override public function activate(onActivated:Function, prevView:IView = null):void
		{
			super.activate(onActivated, prevView);
		}

		override public function deactivate(onDeactivated:Function, nextView:IView = null):void
		{
			destroy();
			super.deactivate(onDeactivated, nextView);
		}

		override public function dispose():void
		{
			destroy();
			super.dispose();
		}


	}
}
