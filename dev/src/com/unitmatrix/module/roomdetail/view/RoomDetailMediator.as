package com.unitmatrix.module.roomdetail.view
{
	import com.sleepydesign.robotlegs.apps.model.AppModel;
	import com.unitmatrix.core.model.CoreModel;
	import com.unitmatrix.core.model.data.BridgeDetailData;

	import flash.events.Event;
	import flash.events.MouseEvent;

	import org.robotlegs.utilities.modular.mvcs.ModuleMediator;

	public class RoomDetailMediator extends ModuleMediator
	{
		// external ------------------------------------------------------------------

		[Inject]
		public var coreModel:CoreModel;

		[Inject]
		public var appModel:AppModel;

		// internal ------------------------------------------------------------------

		[Inject]
		public var module:RoomDetailModule;


		// create ------------------------------------------------------------------

		private var current_page:int;
		private var total_page:int;
		private var row_per_page:int = 10;
		private var _bridgeDetailData:BridgeDetailData;


		override public function onRegister():void
		{
			module.create();
			module.createRoom(coreModel.RoomName);

			initMouseHandler();
		}


		private function initMouseHandler():void
		{
			eventMap.mapListener(module, MouseEvent.MOUSE_UP, onMouseUp);
		}

		// event ------------------------------------------------------------------
		private function onMouseUp(event:Event):void
		{
			if (event.target.name == "close_btn")
			{
				//appModel.viewManager.viewID = Config.M30_RESERVEROOM_MODULE;
				module.dispose();
			}

		}


		// destroy ------------------------------------------------------------------

		override public function onRemove():void
		{
			eventMap.unmapListener(module, MouseEvent.MOUSE_UP, onMouseUp);

			appModel.viewManager.removeViewByID(module.ID);
			appModel = null;
			module = null;
		}
	}
}
