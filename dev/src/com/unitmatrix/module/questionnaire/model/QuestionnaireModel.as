package com.unitmatrix.module.questionnaire.model
{
	import com.unitmatrix.module.questionnaire.signals.RequestInitAppDataSignal;
//	import com.bmmt02.module.welcome.signals.RequestLoginSignal;

	import org.osflash.signals.Signal;
	import org.robotlegs.mvcs.Actor;

	public class QuestionnaireModel extends Actor
	{
		// internal ------------------------------------------------------------------

		[Inject]
		public var requestInitAppDataSignal:RequestInitAppDataSignal;


		// create --------------------------------------------------------------------
		public var loginCompleteSignal:Signal = new Signal(String);

		public var user:String;
		public var pass:String;


		public function getData():void
		{
			requestInitAppDataSignal.dispatch();
		}


	}
}
