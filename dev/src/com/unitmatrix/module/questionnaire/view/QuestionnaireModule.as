package com.unitmatrix.module.questionnaire.view
{

	import com.bit101.components.PushButton;
	import com.sleepydesign.robotlegs.apps.view.IView;
	import com.sleepydesign.robotlegs.modules.ModuleBase;
	import com.unitmatrix.module.questionnaire.QuestionnaireContext;

	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.geom.Rectangle;
	import flash.media.StageWebView;

	import org.robotlegs.core.IInjector;

	public class QuestionnaireModule extends ModuleBase
	{
		private var _from_login:Sprite;
		private var _block:Sprite;

		public var webView:StageWebView;

		[Inject]
		public function set parentInjector(value:IInjector):void
		{
			_context = new QuestionnaireContext(this, value);
		}

		// -----------------------------------------------------------------------------------------------------------------------

		override public function create():void
		{
			graphics.beginFill(0xffffff, 1.0);
			graphics.drawRect(0, 0, 1920, 200);
			graphics.endFill();

			webView = new StageWebView(true);
			webView.stage = this.stage;
			webView.viewPort = new Rectangle(0, 50, stage.stageWidth, stage.stageHeight - 50);

			var testFile:File = File.applicationDirectory.resolvePath("questionnaire.html");
			var testFileCopy:File = File.createTempFile();
			testFile.copyTo(testFileCopy, true);

			var fs:FileStream = new FileStream();
			fs.open(testFile, FileMode.READ);
			var w:String = fs.readUTFBytes(fs.bytesAvailable);
			fs.close();

			webView.loadURL("http://sena.crmthai.net/QS.php");

//			webView.loadString(w);

			var back_btn:PushButton = new PushButton(this, 0, 0, "Back");
			back_btn.scaleX = back_btn.scaleY = 2;
			back_btn.name = "back_btn";

			back_btn.x = 1920 / 2 - back_btn.width / 2;
			back_btn.y = 20;

		}


		override public function activate(onActivated:Function, prevView:IView = null):void
		{

			super.activate(onActivated, prevView);
		}

		override public function deactivate(onDeactivated:Function, nextView:IView = null):void
		{
			dispose();

			super.deactivate(onDeactivated, nextView);
		}

		override public function dispose():void
		{
			webView.stage = null;

			destroy();
			super.dispose();
		}

	}
}
