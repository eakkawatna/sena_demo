package com.unitmatrix.module.questionnaire.remote.service
{
	import com.unitmatrix.core.component.DialogComponent;
	import com.unitmatrix.core.model.CoreModel;
	import com.unitmatrix.core.model.data.BridgeData;
	import com.unitmatrix.core.paser.Parser;
	import com.unitmatrix.module.questionnaire.model.QuestionnaireModel;
	import com.unitmatrix.module.questionnaire.signals.ReceiveInitAppDataSignal;
	import com.idealizm.manager.LoadManager;

	import flash.net.URLVariables;

	import org.robotlegs.mvcs.Actor;

	public class QuestionnaireService extends Actor
	{

		[Inject]
		public var coreModel:CoreModel;

		[Inject]
		public var model:QuestionnaireModel;

		[Inject]
		public var receiveInitAppDataSignal:ReceiveInitAppDataSignal;

		private var _counter:int = 0;
		private var _version:String;
		private var _old_raw:String;
		private var _old_version:String;
		private var _raw:String;
		private var _isNew:Boolean;
		private var _xml:XML;
		private var _dialogComponent:DialogComponent;

		public function init():void
		{
			loadAppData();
		}

		private function loadAppData():void
		{
			LoadManager.loadJson(Config.SERVER_DATA + "bmmt_ai/JsonData1.php", new URLVariables).addOnce(onLoadStep1);
		}

		private function onLoadStep1(raw:String):void
		{
			var rawDatas:Vector.<BridgeData> = Parser.parseBridgeData(raw);
			var newDatas:Vector.<BridgeData> = new Vector.<BridgeData>();

			if (!coreModel.currentUserData.groupid || coreModel.currentUserData.groupid == "")
			{
				coreModel.bridgeDatas = rawDatas;
			}
			else
			{
				for each (var bridgeData:BridgeData in rawDatas)
				{
					if (coreModel.currentUserData.groupid == bridgeData.group_id)
						newDatas.push(bridgeData);
				}

				coreModel.bridgeDatas = newDatas;
			}

			coreModel.bridgeDatas
			if (!coreModel.bridgeDatas)
			{
				systemError();
				return;
			}

			LoadManager.loadJson(Config.SERVER_DATA + "bmmt_ai/JsonData2.php", new URLVariables).addOnce(onLoadStep2);

		}

		private function systemError():void
		{
			coreModel.systemErrorSignal.dispatch();
		}

		private function onLoadStep2(raw:String):void
		{
			coreModel.treatmentDatas = Parser.parseTreatmentData(raw);
			if (!coreModel.treatmentDatas)
			{
				systemError();
				return;
			}

			LoadManager.loadJson(Config.SERVER_DATA + "bmmt_ai/JsonData3.php", new URLVariables).addOnce(onLoadStep3);

		}

		private function onLoadStep3(raw:String):void
		{
			coreModel.bridgeDetailDatas = Parser.parseBridgeDetailData(raw);
			if (!coreModel.bridgeDetailDatas)
			{
				systemError();
				return;
			}

			LoadManager.loadJson(Config.SERVER_DATA + "bmmt_ai/jsondata4Search.php", new URLVariables).addOnce(onLoadComplete);

		}

		private function onLoadComplete(raw:String):void
		{
			coreModel.searchData = Parser.parseSearchData(raw);

			coreModel.saveLocal();
			receiveInitAppDataSignal.dispatch();
		}

	}
}
