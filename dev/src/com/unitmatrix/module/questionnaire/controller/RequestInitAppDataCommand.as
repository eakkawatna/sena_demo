package com.unitmatrix.module.questionnaire.controller
{
	import com.unitmatrix.module.questionnaire.remote.service.QuestionnaireService;

	import org.robotlegs.mvcs.SignalCommand;

	public class RequestInitAppDataCommand extends SignalCommand
	{
		[Inject]
		public var service:QuestionnaireService;


		//-get carees data from server.
		override public function execute():void
		{
			service.init();
			//-
			super.execute();
		}
	}
}
