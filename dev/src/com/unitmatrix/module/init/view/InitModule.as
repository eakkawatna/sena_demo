package com.unitmatrix.module.init.view
{

	import com.unitmatrix.module.init.InitContext;
	import com.sleepydesign.robotlegs.apps.view.IView;
	import com.sleepydesign.robotlegs.modules.ModuleBase;

	import flash.display.Sprite;

	import org.robotlegs.core.IInjector;

	public class InitModule extends ModuleBase
	{
		private var _from_login:Sprite;
		private var _block:Sprite;

		[Inject]
		public function set parentInjector(value:IInjector):void
		{
			_context = new InitContext(this, value);
		}

		// -----------------------------------------------------------------------------------------------------------------------


		public function InitModule()
		{

		}

		override public function create():void
		{

		}


		override public function activate(onActivated:Function, prevView:IView = null):void
		{

			super.activate(onActivated, prevView);
		}

		override public function deactivate(onDeactivated:Function, nextView:IView = null):void
		{
			super.deactivate(onDeactivated, nextView);
		}

		override public function dispose():void
		{
			destroy();
			super.dispose();
		}

	}
}
