package com.unitmatrix.module.init.view
{
	import com.sleepydesign.lightboxs.signals.LightBoxSignal;
	import com.sleepydesign.robotlegs.apps.model.AppModel;
	import com.unitmatrix.core.model.CoreModel;

	import org.robotlegs.utilities.modular.mvcs.ModuleMediator;


	public class InitMediator extends ModuleMediator
	{
		// external ------------------------------------------------------------------

		[Inject]
		public var coreModel:CoreModel;

		[Inject]
		public var appModel:AppModel;

		[Inject]
		public var lightBoxSignal:LightBoxSignal;

		// internal ------------------------------------------------------------------

		[Inject]
		public var module:InitModule;

		// create ------------------------------------------------------------------

		override public function onRegister():void
		{
//			redirect to home view.
			appModel.viewManager.viewID = Config.M20_HOME_MODULE;
		}

		// event ------------------------------------------------------------------

		// destroy ------------------------------------------------------------------

		override public function onRemove():void
		{
			appModel.viewManager.removeViewByID(module.ID);
			appModel = null;
			module = null;
		}
	}
}
