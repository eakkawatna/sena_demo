package com.unitmatrix.core.model.data
{

	public class BridgeDetailData extends Object
	{
		public var id:String;
		public var path:String;
		public var bridgename:String;
		public var positionlat:String;
		public var positionlng:String;
		public var size:String;
		public var length:String;
		public var tambol_id:String;
		public var amphur_id:String;
		public var province_id:String;
		public var groupid:String;
		public var highwayid:String;
		public var controlpath:String;
		public var categoryid:String;
		public var lane:String;
		public var environment:String;
		public var direction:String;
		public var structuretoptype:String;
		public var structuretopmaterial:String;
		public var structurebottomtype:String;
		public var structurebottommaterial:String;
		public var propertytop:String;
		public var propertybottomid:String;
		public var surfaceid:String;
		public var createdate:String;
		public var divisionid:String;
		public var position:String;
		public var sealavel:String;
		public var aadt:String;
		public var aadttruck:String;
		public var aadtyear:String;
		public var typeid:String;
		public var pierid:String;
		public var slopeprotectionid:String;
		public var width:String;
		public var span:String;
		public var skew:String;
		public var addempcd:String;
		public var addpcnm:String;
		public var adddt:String;
		public var updempcd:String;
		public var updpcnm:String;
		public var upddt:String;
		public var surveytypeid:String;
		public var surfacewidth:String;
		public var damagelevel:String;
		public var mapimage:String;
		public var sequencecode:String;
		public var quickrepairflg:String;
		public var bpiscore:String;
		public var leftroadside:String;
		public var rightroadside:String;
		public var bridgerailing:String;
		public var material:String;
		public var weightdesign:String;
		public var controlpathid:String;
		public var controlpathname:String;
		public var categoryname:String;
		public var categoryabbrev:String;
		public var tambol_code:String;
		public var tambol_name:String;
		public var delete_data:String;
		public var amphur_code:String;
		public var amphur_name:String;
		public var province_code:String;
		public var province_name:String;
		public var divisionname:String;
		public var divisionabbrev:String;
		public var surfacename:String;
		public var surfaceabbrev:String;
		public var surveytypecode:String;
		public var surveytypename:String;
		public var groupname:String;
		public var groupabbrev:String;
		public var typename:String;
		public var typeabbrev:String;
		public var piername:String;
		public var pierabbrev:String;
		public var slopeprotectionname:String;
		public var slopeprotectionabbrev:String;
		public var highwaycode:String;
		public var highwayname:String;
		public var propertybottomname:String;
		public var directionid:String;
		public var directionname:String;
		public var vehicledetail:Vector.<VehicleData>
		public var repairdetail:Array;

		public function toObject():Object
		{
			var vehicledetails:Array = [];
			for each (var vd:VehicleData in vehicledetail)
			{
				vehicledetails.push(vd.toObject());
			}
			return {path: path, bridgeid: id, bridgename: bridgename, positionlat: positionlat, positionlng: positionlng, size: size, length: length, tambol_id: tambol_id, amphur_id: amphur_id, province_id: province_id, groupid: groupid, highwayid: highwayid, controlpath: controlpath, categoryid: categoryid, lane: lane, environment: environment, direction: direction, structuretoptype: structuretoptype, structuretopmaterial: structuretopmaterial, structurebottomtype: structurebottomtype, structurebottommaterial: structurebottommaterial, propertytop: propertytop, propertybottomid: propertybottomid, surfaceid: surfaceid, createdate: createdate, divisionid: divisionid, position: position, sealavel: sealavel, aadt: aadt, aadttruck: aadttruck, aadtyear: aadtyear, typeid: typeid, pierid: pierid, slopeprotectionid: slopeprotectionid, width: width, span: span, skew: skew, addempcd: addempcd, addpcnm: addpcnm, adddt: adddt, updempcd: updempcd, updpcnm: updpcnm, upddt: upddt, surveytypeid: surveytypeid, surfacewidth: surfacewidth, damagelevel: damagelevel, mapimage: mapimage, sequencecode: sequencecode, quickrepairflg: quickrepairflg, bpiscore: bpiscore, leftroadside: leftroadside, rightroadside: rightroadside, bridgerailing: bridgerailing, material: material, weightdesign: weightdesign, controlpathid: controlpathid, controlpathname: controlpathname, categoryname: categoryname, categoryabbrev: categoryabbrev, tambol_code: tambol_code, tambol_name: tambol_name, "delete": delete_data, amphur_code: amphur_code, amphur_name: amphur_name, province_code: province_code, province_name: province_name, divisionname: divisionname, divisionabbrev: divisionabbrev, surfacename: surfacename, surfaceabbrev: surfaceabbrev, surveytypecode: surveytypecode, surveytypename: surveytypename, groupname: groupname, groupabbrev: groupabbrev, typename: typename, typeabbrev: typeabbrev, piername: piername, pierabbrev: pierabbrev, slopeprotectionname: slopeprotectionname, slopeprotectionabbrev: slopeprotectionabbrev, highwaycode: highwaycode, highwayname: highwayname, propertybottomname: propertybottomname, directionid: directionid, directionname: directionname, vehicledetail: vehicledetails, repairdetail: repairdetail};
		}
	}
}
