package com.unitmatrix.core.model.data
{

	public class VehicleData extends Object
	{
		private var _id:String;
		private var _path:String;
		private var _name:String;
		private var _qty:String;

		public function VehicleData(id:String, name:String, qty:String, path:String = "")
		{
			_id = id;
			_name = name;
			_qty = qty;
			if (path == "")
				_path = String(new Date().time / 1000000000).split(".")[1] + "" + Math.round(Math.random() * 999);
			else
				_path = path;
		}

		public function get path():String
		{
			return _path;
		}

		public function set path(value:String):void
		{
			_path = value;
		}

		public function get id():String
		{
			return _id;
		}

		public function get name():String
		{
			return _name;
		}

		public function get qty():String
		{
			return _qty;
		}


		public function toObject():Object
		{
			return {vehicleid: _id, vehiclenm: _name, vehicleqty: _qty};
		}
	}
}
