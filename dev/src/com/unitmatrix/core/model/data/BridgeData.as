package com.unitmatrix.core.model.data
{

	public class BridgeData extends Object
	{
		private var _id:String;
		private var _bridge_name:String;
		private var _group_id:String;
		private var _group_name:String;
		private var _position:String;
		private var _direction:String;
		private var _direction_name:String;
		private var _highway_id:String;
		private var _highway_name:String;
		private var _damage_level:String;
		private var _bpi_score:String;
		private var _span:String;
		private var _create_date:String;
		private var _category_id:String;
		private var _type_name:String;
		private var _category_name:String;
		private var _routine_detail:Vector.<RoutineDetail>;
		private var _printciple_detail:Vector.<PrintcipleDetail>;
		private var _path:String;

		public var isSave:Boolean = false;

		public function BridgeData(id:String, bridge_name:String, group_id:String, group_name:String, position:String, direction:String, direction_name:String, highway_id:String, highway_name:String, damage_level:String, bpi_score:String, span:String, create_date:String, category_id:String, type_name:String, category_name:String, routine_detail:Vector.<RoutineDetail>, printciple_detail:Vector.<PrintcipleDetail>, path:String = "")
		{
			_id = id;
			_bridge_name = bridge_name;
			_group_id = group_id;
			_group_name = group_name;
			_position = position;
			_direction = direction;
			_direction_name = direction_name;
			_highway_id = highway_id;
			_highway_name = highway_name;
			_damage_level = damage_level;
			_bpi_score = bpi_score;
			_span = span;
			_create_date = create_date;
			_category_id = category_id;
			_type_name = type_name;
			_category_name = category_name;
			_routine_detail = routine_detail;
			_printciple_detail = printciple_detail;

			if (path == "")
				_path = String(new Date().time / 1000000000).split(".")[1] + "" + Math.round(Math.random() * 999);
			else
				_path = path;
		}

		public function get id():String
		{
			return _id;
		}

		public function get group_id():String
		{
			return _group_id;
		}

		public function get group_name():String
		{
			return _group_name;
		}

		public function get position():String
		{
			return _position;
		}

		public function get direction():String
		{
			return _direction;
		}

		public function get direction_name():String
		{
			return _direction_name;
		}

		public function get highway_id():String
		{
			return _highway_id;
		}

		public function get highway_name():String
		{
			return _highway_name;
		}

		public function get damage_level():String
		{
			return _damage_level;
		}

		public function get bpi_score():String
		{
			return _bpi_score;
		}

		public function get span():String
		{
			return _span;
		}

		public function get create_date():String
		{
			return _create_date;
		}

		public function get category_id():String
		{
			return _category_id;
		}

		public function get type_name():String
		{
			return _type_name;
		}

		public function get category_name():String
		{
			return _category_name;
		}

		public function get routine_detail():Vector.<RoutineDetail>
		{
			return _routine_detail;
		}

		public function get printciple_detail():Vector.<PrintcipleDetail>
		{
			return _printciple_detail;
		}

		public function set id(value:String):void
		{
			_id = value;
		}

		public function get bridge_name():String
		{
			return _bridge_name;
		}

		public function toObject():Object
		{
			var _routines:Array = [];
			for each (var rd:RoutineDetail in _routine_detail)
			{
				_routines.push(rd.toObject());
			}
			var _printciples:Array = [];
			for each (var rp:PrintcipleDetail in _printciple_detail)
			{
				_printciples.push(rp.toObject());
			}
			return {bridgeid: _id, bridgename: _bridge_name, groupid: _group_id, groupname: _group_name, position: _position, direction: _direction, directionname: _direction_name, highwayid: _highway_id, highwayname: _highway_name, damagelevel: _damage_level, bpiscore: _bpi_score, span: _span, createdate: _create_date, categoryid: _category_id, typename: _type_name, categoryname: _category_name, routinedetail: _routines, printcipledetail: _printciples, isSave: isSave};
		}
	}
}
