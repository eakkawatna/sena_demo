package com.unitmatrix.core.model.data
{

	public class PrintcipleDetail extends Object
	{
		private var _id:String;
		private var _principle_dt:String;
		private var _principle_last_dt:String;
		private var _principle_next_dt:String;
		private var _superstruct_rate:String;
		private var _substruct_rate:String;
		private var _bridgeneck_rate:String;
		private var _slopeprotection_rate:String;
		private var _surface_point:String;
		private var _principle_special_flag:String;
		private var _memo_detail:Vector.<MemoDetail>;
		private var _treatment_detail:Vector.<TreatmentDetailData>;
		private var _path:String;
		private var _addstt:String;

		public function PrintcipleDetail(id:String, principle_dt:String, principle_last_dt:String, principle_next_dt:String, superstruct_rate:String, substruct_rate:String, bridgeneck_rate:String, slopeprotection_rate:String, surface_point:String, principle_special_flag:String, memo_detail:Vector.<MemoDetail>, treatment_detail:Vector.<TreatmentDetailData>, addstt:String, path:String = "")
		{
			_id = id;

			if (_id == null)
				_id = String(new Date().time / 1000000000).split(".")[1] + "" + Math.round(Math.random() * 999);

			_principle_dt = principle_dt;
			_principle_last_dt = principle_last_dt;
			_principle_next_dt = principle_next_dt;
			_superstruct_rate = (!superstruct_rate ||superstruct_rate == "") ? "n" : superstruct_rate;
			_substruct_rate = (!substruct_rate ||substruct_rate == "" )? "n" : substruct_rate;
			_bridgeneck_rate = (!bridgeneck_rate ||bridgeneck_rate == "")? "n" : bridgeneck_rate;
			_slopeprotection_rate = (!slopeprotection_rate ||slopeprotection_rate == "")? "n" : slopeprotection_rate;
			_surface_point = (!surface_point ||surface_point == "") ? "n" : surface_point;
			_principle_special_flag = principle_special_flag;
			_memo_detail = memo_detail;
			_treatment_detail = treatment_detail;

			if ((_superstruct_rate != "n" && int(_superstruct_rate) < 3) || (_substruct_rate != "n" && int(_substruct_rate) < 3) || (_bridgeneck_rate != "n" && int(_bridgeneck_rate) < 3) || (_slopeprotection_rate != "n" && int(_slopeprotection_rate) < 3) || (_surface_point != "n" && int(_surface_point) < 3))
				addstt = "1";
			else
				addstt = "0";

			_addstt = addstt;

			if (path == "")
				_path = String(new Date().time / 1000000000).split(".")[1] + "" + Math.round(Math.random() * 999);
			else
				_path = path;
		}

		public function get path():String
		{
			return _path;
		}

		public function set path(value:String):void
		{
			_path = value;
		}

		public function set treatment_detail(value:Vector.<TreatmentDetailData>):void
		{
			_treatment_detail = value;
		}

		public function set memo_detail(value:Vector.<MemoDetail>):void
		{
			_memo_detail = value;
		}

		public function set principle_special_flag(value:String):void
		{
			_principle_special_flag = value;
		}

		public function set surface_point(value:String):void
		{
			_surface_point = value;
		}

		public function set slopeprotection_rate(value:String):void
		{
			_slopeprotection_rate = value;
		}

		public function set bridgeneck_rate(value:String):void
		{
			_bridgeneck_rate = value;
		}

		public function set substruct_rate(value:String):void
		{
			_substruct_rate = value;
		}

		public function set superstruct_rate(value:String):void
		{
			_superstruct_rate = value;
		}

		public function set principle_next_dt(value:String):void
		{
			_principle_next_dt = value;
		}

		public function set principle_last_dt(value:String):void
		{
			_principle_last_dt = value;
		}

		public function set principle_dt(value:String):void
		{
			_principle_dt = value;
		}

		public function get id():String
		{
			return _id;
		}

		public function get principle_dt():String
		{
			return _principle_dt;
		}

		public function get principle_last_dt():String
		{
			return _principle_last_dt;
		}

		public function get principle_next_dt():String
		{
			return _principle_next_dt;
		}

		public function get superstruct_rate():String
		{
			return _superstruct_rate;
		}

		public function get substruct_rate():String
		{
			return _substruct_rate;
		}

		public function get bridgeneck_rate():String
		{
			return _bridgeneck_rate;
		}

		public function get slopeprotection_rate():String
		{
			return _slopeprotection_rate;
		}

		public function get surface_point():String
		{
			return _surface_point;
		}

		public function get principle_special_flag():String
		{
			return _principle_special_flag;
		}


		public function get treatment_detail():Vector.<TreatmentDetailData>
		{
			return _treatment_detail;
		}

		public function get memo_detail():Vector.<MemoDetail>
		{
			return _memo_detail;
		}

		public function get addstt():String
		{
			return _addstt;
		}

		public function set addstt(value:String):void
		{
			_addstt = value;
		}

		public function toObject():Object
		{
			var _memos:Array = [];
			for each (var md:MemoDetail in _memo_detail)
			{
				_memos.push(md.toObject());
			}
			var _treatments:Array = [];
			for each (var td:TreatmentDetailData in _treatment_detail)
			{
				_treatments.push(td.toObject());
			}

			return {principleid: _id, principledt: _principle_dt, principlelastdt: _principle_last_dt, principlenextdt: _principle_next_dt, superstructrate: _superstruct_rate, substructrate: _substruct_rate, bridgeneckrate: _bridgeneck_rate, slopeprotectionrate: _slopeprotection_rate, surfacepoint: _surface_point, principlespecialflag: _principle_special_flag, memodetail: _memos, treatmentdetail: _treatments, addstt: _addstt};

		}


	}
}


