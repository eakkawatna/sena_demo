package com.unitmatrix.core.component
{
	import com.sleepydesign.display.SDSprite;

	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;

	import org.osflash.signals.Signal;

	public class DialogComponent extends SDSprite
	{
		public static const DIALOGCOMPONENT_CLICK_YES:String = "yes";
		public static const DIALOGCOMPONENT_CLICK_NO:String = "no";
		public static const DIALOGCOMPONENT_CLICK_CLOSE:String = "close";

		public static const CLOSE:String = "close";
		public static const YES_NO:String = "yse_no";
		public static const NONE:String = "none";

		private var _skin:Sprite;
		private var _title:String;
		private var _desc:String;
		private var _type:String;
		private var _onClickSignal:Signal;

		/**
		 *
		 * @param title
		 * @param desc
		 * @param type
		 * @param onClickSignal (String)
		 *
		 */
		public function DialogComponent(title:String, desc:String, type:String = NONE, onClickSignal:Signal = null)
		{
			addEventListener(Event.ADDED_TO_STAGE, onStage);
			_title = title;
			_desc = desc;
			_type = type;

			_onClickSignal = onClickSignal;
		}

		protected function onStage(event:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, onStage);

			create();

			if (_onClickSignal)
				addEventListener(MouseEvent.MOUSE_UP, onMouseUp);
		}

		protected function onMouseUp(event:MouseEvent):void
		{


		}

		private function create():void
		{

		}


		//destroy ---------------------------------------------

		public function dispose():void
		{
			/*if (_onClickSignal)
				removeEventListener(MouseEvent.MOUSE_UP, onMouseUp);*/
			_onClickSignal = null;
			destroy();
		}
	}
}
