package com.unitmatrix.core.component
{
	import com.greensock.TweenLite;
	import com.sleepydesign.display.SDSprite;

	import flash.display.Sprite;
	import flash.events.Event;
	import flash.text.TextField;

	import Dialog.upload_sp;

	import SurveyInfo.block_image;

	public class UploadDialogComponent extends SDSprite
	{

		private var _skin:Sprite;
		private var _title:String;
		private var _desc:String;
		private var _progress_value:int;

		/**
		 *
		 * @param title
		 * @param desc
		 * @param progress_value
		 *
		 */

		public function UploadDialogComponent(title:String, desc:String)
		{
			addEventListener(Event.ADDED_TO_STAGE, onStage);
			_title = title;
			_desc = desc;
		}

		protected function onStage(event:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, onStage);

			create();
		}

		private function create():void
		{
			var container:Sprite = new Sprite();
			addChild(container);
			container.graphics.beginBitmapFill(new SurveyInfo.block_image, null, true, false);
			container.graphics.drawRect(0, 0, Config.GUI_SIZE.width, Config.GUI_SIZE.height);
			container.graphics.endFill();
			container.alpha = 0.8;

			_skin = new Dialog.upload_sp;
			addChild(_skin);


			title_tf.text = _title;
			desc_tf.htmlText = _desc;
			//progress_value = 0;
			resize();
		}

		private function resize():void
		{
			_skin.scaleX = _skin.scaleY = Config.ratio;
			_skin.x = Config.GUI_SIZE.width * 0.5 //- _skin.width * 0.5;
			_skin.y = Config.GUI_SIZE.height * 0.5 // - _skin.height * 0.5;
		}

		private function get title_tf():TextField
		{
			return _skin.getChildByName("title_tf") as TextField;
		}

		private function get desc_tf():TextField
		{
			return _skin.getChildByName("desc_tf") as TextField;
		}

		private function get percent_tf():TextField
		{
			return _skin.getChildByName("percent_tf") as TextField;
		}

		private function get progress_sp():Sprite
		{
			return _skin.getChildByName("progress_sp") as Sprite;
		}


		//destroy ---------------------------------------------

		public function dispose():void
		{
			TweenLite.killTweensOf(progress_sp);
			destroy();
		}

		public function get progress_value():int
		{
			return _progress_value;
		}

		public function set progress_value(value:int):void
		{
			_progress_value = value;
			percent_tf.text = _progress_value.toString() + "%";

			if (_progress_value == 0)
			{
				progress_sp.scaleX = 0;
			}
			else if (_progress_value > -1 && _progress_value <= 100)
			{
				TweenLite.killTweensOf(progress_sp);
				TweenLite.to(progress_sp, 1, {scaleX: _progress_value / 100});
			}

			if (_progress_value == 100)
				dispose();

		}

	}
}
