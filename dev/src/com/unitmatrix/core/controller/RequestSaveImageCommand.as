package com.unitmatrix.core.controller
{
	import com.unitmatrix.core.remote.service.CoreService;

	import flash.utils.ByteArray;

	import org.robotlegs.mvcs.SignalCommand;

	public class RequestSaveImageCommand extends SignalCommand
	{
		[Inject]
		public var service:CoreService;

		[Inject(name = 'ba')]
		public var ba:ByteArray;

		//-get carees data from server.
		override public function execute():void
		{
			service.saveImage(ba);
			super.execute();
		}
	}
}
