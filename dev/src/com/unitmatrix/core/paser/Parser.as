package com.unitmatrix.core.paser
{
	import com.unitmatrix.core.model.data.BridgeData;
	import com.unitmatrix.core.model.data.BridgeDetailData;
	import com.unitmatrix.core.model.data.CategoryData;
	import com.unitmatrix.core.model.data.GroupData;
	import com.unitmatrix.core.model.data.HighwayData;
	import com.unitmatrix.core.model.data.MemoDetail;
	import com.unitmatrix.core.model.data.PrintcipleDetail;
	import com.unitmatrix.core.model.data.RoutineDetail;
	import com.unitmatrix.core.model.data.SearchData;
	import com.unitmatrix.core.model.data.SurfaceData;
	import com.unitmatrix.core.model.data.TreatmentData;
	import com.unitmatrix.core.model.data.TreatmentDetailData;
	import com.unitmatrix.core.model.data.VehicleData;
	import com.sleepydesign.utils.JSONUtil;

	public class Parser
	{

		public static function parseBridgeData(raw:String):Vector.<BridgeData>
		{
			var bridgeDatas:Vector.<BridgeData> = new Vector.<BridgeData>;
			var rawObj:Array = JSONUtil.decode(raw);

			if (!rawObj)
				return null;

			for each (var obj:Object in rawObj)
			{
				var data:BridgeData = new BridgeData(obj.bridgeid, obj.bridgename, obj.groupid, obj.groupname, obj.position, obj.direction, obj.directionname, obj.highwayid, obj.highwayname, obj.damagelevel, obj.bpiscore, obj.span, obj.createdate, obj.categoryid, obj.typename, obj.categoryname, parseRoutineDetailData(obj.routinedetail), parsePrintcipleDetailData(obj.printcipledetail))
				data.isSave = obj.isSave;
				bridgeDatas.push(data);
			}
			return bridgeDatas;
		}


		public static function parseTreatmentDetailData(raw:Array):Vector.<TreatmentDetailData>
		{
			var treatmentDetail:Vector.<TreatmentDetailData> = new Vector.<TreatmentDetailData>;

			for each (var obj:Object in raw)
			{
				var data:TreatmentDetailData = new TreatmentDetailData(obj.treatmentid, obj.treatmentqty, obj.path);
				treatmentDetail.push(data);
			}
			return treatmentDetail;
		}

		public static function parseRoutineDetailData(raw:Array):Vector.<RoutineDetail>
		{
			var routineDetails:Vector.<RoutineDetail> = new Vector.<RoutineDetail>;

			for each (var obj:Object in raw)
			{
				var data:RoutineDetail = new RoutineDetail(obj.routinid, obj.routinedt, obj.routinevlastdt, obj.routinenextdt, obj.routinespecialflag, parseMemoDetailData(obj.memodetail), parseTreatmentDetailData(obj.treatmentdetail));
				routineDetails.push(data);
			}
			return routineDetails;
		}

		public static function parsePrintcipleDetailData(raw:Array):Vector.<PrintcipleDetail>
		{
			var printcipleDetails:Vector.<PrintcipleDetail> = new Vector.<PrintcipleDetail>;

			for each (var obj:Object in raw)
			{
				var data:PrintcipleDetail = new PrintcipleDetail(obj.principleid, obj.principledt, obj.principlelastdt, obj.principlenextdt, obj.superstructrate, obj.substructrate, obj.bridgeneckrate, obj.slopeprotectionrate, obj.surface_point, obj.principlespecialflag, parseMemoDetailData(obj.memodetail), parseTreatmentDetailData(obj.treatmentdetail), obj.addstt);
				printcipleDetails.push(data);
			}
			return printcipleDetails;
		}

		public static function parseMemoDetailData(raw:Array):Vector.<MemoDetail>
		{
			var memoDetails:Vector.<MemoDetail> = new Vector.<MemoDetail>;

			for each (var obj:Object in raw)
			{
				var data:MemoDetail = new MemoDetail(obj.detailid, obj.memo, obj.path);
				memoDetails.push(data);
			}
			return memoDetails;
		}


		public static function parseTreatmentData(raw:String):Vector.<TreatmentData>
		{
			var treatmentDatas:Vector.<TreatmentData> = new Vector.<TreatmentData>;
			var rawObj:Array = JSONUtil.decode(raw);
			if (!rawObj)
				return null;


			for each (var obj:Object in rawObj)
			{
				var data:TreatmentData = new TreatmentData(obj.treatmentid, obj.treatment_name, obj.treatmentcode, obj.routineflag, obj.principleflag, obj.treatment_unit);
				treatmentDatas.push(data);
			}
			return treatmentDatas;
		}

		public static function parseBridgeDetailData(raw:String):Vector.<BridgeDetailData>
		{
			var bridgeDetailDatas:Vector.<BridgeDetailData> = new Vector.<BridgeDetailData>;
			var rawObj:Array = JSONUtil.decode(raw);
			if (!rawObj)
				return null;

			for each (var obj:Object in rawObj)
			{
				var data:BridgeDetailData = new BridgeDetailData();
				data.id = obj.bridgeid;
				data.bridgename = obj.bridgename;
				data.positionlat = obj.positionlat;
				data.positionlng = obj.positionlng;
				data.size = obj.size;
				data.length = obj.length;
				data.tambol_id = obj.tambol_id;
				data.amphur_id = obj.amphur_id;
				data.province_id = obj.province_id;
				data.groupid = obj.groupid;
				data.highwayid = obj.highwayid;
				data.controlpath = obj.controlpath;
				data.categoryid = obj.categoryid;
				data.lane = obj.lane;
				data.environment = obj.environment;
				data.direction = obj.direction;
				data.structuretoptype = obj.structuretoptype;
				data.structuretopmaterial = obj.structuretopmaterial;
				data.structurebottomtype = obj.structurebottomtype;
				data.structurebottommaterial = obj.structurebottommaterial;
				data.propertytop = obj.propertytop;
				data.propertybottomid = obj.propertybottomid;
				data.surfaceid = obj.surfaceid;
				data.createdate = obj.createdate;
				data.divisionid = obj.divisionid;
				data.position = obj.position;
				data.sealavel = obj.sealavel;
				data.aadt = obj.aadt;
				data.aadttruck = obj.aadttruck;
				data.aadtyear = obj.aadtyear;
				data.typeid = obj.typeid;
				data.pierid = obj.pierid;
				data.slopeprotectionid = obj.slopeprotectionid;
				data.width = obj.width;
				data.span = obj.span;
				data.skew = obj.skew;
				data.addempcd = obj.addempcd;
				data.addpcnm = obj.addpcnm;
				data.adddt = obj.adddt;
				data.updempcd = obj.updempcd;
				data.updpcnm = obj.updpcnm;
				data.upddt = obj.upddt;
				data.surveytypeid = obj.surveytypeid;
				data.surfacewidth = obj.surfacewidth;
				data.damagelevel = obj.damagelevel;
				data.mapimage = obj.mapimage;
				data.sequencecode = obj.sequencecode;
				data.quickrepairflg = obj.quickrepairflg;
				data.bpiscore = obj.bpiscore;
				data.leftroadside = obj.leftroadside;
				data.rightroadside = obj.rightroadside;
				data.bridgerailing = obj.bridgerailing;
				data.material = obj.material;
				data.weightdesign = obj.weightdesign;
				data.controlpathid = obj.controlpathid;
				data.controlpathname = obj.controlpathname;
				data.categoryname = obj.categoryname;
				data.categoryabbrev = obj.categoryabbrev;
				data.tambol_code = obj.tambol_code;
				data.tambol_name = obj.tambol_name;
				data.delete_data = obj.delete_data;
				data.amphur_code = obj.amphur_code;
				data.amphur_name = obj.amphur_name;
				data.province_code = obj.province_code;
				data.province_name = obj.province_name;
				data.divisionname = obj.divisionname;
				data.divisionabbrev = obj.divisionabbrev;
				data.surfacename = obj.surfacename;
				data.surfaceabbrev = obj.surfaceabbrev;
				data.surveytypecode = obj.surveytypecode;
				data.surveytypename = obj.surveytypename;
				data.groupname = obj.groupname;
				data.groupabbrev = obj.groupabbrev;
				data.typename = obj.typename;
				data.typeabbrev = obj.typeabbrev;
				data.piername = obj.piername;
				data.pierabbrev = obj.pierabbrev;
				data.slopeprotectionname = obj.slopeprotectionname;
				data.slopeprotectionabbrev = obj.slopeprotectionabbrev;
				data.highwaycode = obj.highwaycode;
				data.highwayname = obj.highwayname;
				data.propertybottomname = obj.propertybottomname;
				data.directionid = obj.directionid;
				data.directionname = obj.directionname;
				data.vehicledetail = parseVehicleData(obj.vehicledetail);
				data.repairdetail = obj.repairdetail;
				bridgeDetailDatas.push(data);
			}

			return bridgeDetailDatas;
		}


		public static function parseVehicleData(raw:Array):Vector.<VehicleData>
		{
			var vehscleDatas:Vector.<VehicleData> = new Vector.<VehicleData>;

			for each (var obj:Object in raw)
			{
				var data:VehicleData = new VehicleData(obj.vehicleid, obj.vehiclenm, obj.vehicleqty);
				vehscleDatas.push(data);
			}
			return vehscleDatas;
		}

		public static function parseSearchData(raw:String):SearchData
		{
			var rawObj:Object = JSONUtil.decode(raw);
			if (!rawObj)
				return null;

			var data:SearchData = new SearchData(parseGroupData(rawObj.group), parseSurfaceData(rawObj.surface), parseHighwayData(rawObj.highway), parseCategoryData(rawObj.category), parseBPIScore(rawObj.bpiscore));
			return data;
		}

		public static function parseGroupData(raw:Array):Vector.<GroupData>
		{
			var groupDatas:Vector.<GroupData> = new Vector.<GroupData>;

			for each (var obj:Object in raw)
			{
				var data:GroupData = new GroupData(obj.groupid, obj.groupname, obj.groupabbrev);
				groupDatas.push(data);
			}
			return groupDatas;
		}

		public static function parseSurfaceData(raw:Array):Vector.<SurfaceData>
		{
			var surfaceDatas:Vector.<SurfaceData> = new Vector.<SurfaceData>;

			for each (var obj:Object in raw)
			{
				var data:SurfaceData = new SurfaceData(obj.surfaceid, obj.surfacename, obj.surfaceabbrev);
				surfaceDatas.push(data);
			}
			return surfaceDatas;
		}

		public static function parseHighwayData(raw:Array):Vector.<HighwayData>
		{
			var highwayDatas:Vector.<HighwayData> = new Vector.<HighwayData>;
			for each (var obj:Object in raw)
			{
				var data:HighwayData = new HighwayData(obj.highwayid, obj.highwayname, obj.highwaycode);
				highwayDatas.push(data);
			}
			return highwayDatas;
		}

		public static function parseCategoryData(raw:Array):Vector.<CategoryData>
		{
			var categoryDatas:Vector.<CategoryData> = new Vector.<CategoryData>;

			for each (var obj:Object in raw)
			{
				var data:CategoryData = new CategoryData(obj.categoryid, obj.categoryname, obj.categoryabbrev);
				categoryDatas.push(data);
			}
			return categoryDatas;
		}

		public static function parseBPIScore(raw:Object):Vector.<String>
		{
			var scores:Vector.<String> = new Vector.<String>;

			for (var i:String in raw)
			{
				scores.push(raw[i]);
			}
			return scores;
		}
	}
}


