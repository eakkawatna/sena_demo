package com.idealizm.manager
{
	import com.sleepydesign.net.AssetUtil;
	import com.sleepydesign.net.LoaderUtil;

	import flash.display.Loader;
	import flash.display.MovieClip;
	import flash.display.Sprite;

	public class AssetsManager
	{
		public function AssetsManager()
		{
		}

		public static function loadAssets(path:String, name:String, onAssetsComplete:Function):void
		{
			AssetUtil.loadSWF(path, name, Config.VERSION, onAssetsComplete);
		}

		public static function loadImage(path:String, onAssetsComplete:Function):Loader
		{
			return LoaderUtil.loadBinaryAsBitmap(path, onAssetsComplete);
		}

		public static function getClass(groupID:String, assetID:String):Class
		{
			return AssetUtil.getClass(groupID, assetID);

		}

		public static function getSprite(groupID:String, assetID:String):Sprite
		{
			return AssetUtil.getSprite(groupID, assetID);

		}

		public static function getMovieClip(groupID:String, assetID:String):MovieClip
		{
			return AssetUtil.getMovieClip(groupID, assetID);

		}
	}
}
